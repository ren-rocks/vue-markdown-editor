import {
    createMachine,
    state,
    transition,
    interpret,
    state as final 
} from 'robot3';

const TRANSITION_KEYS = Object.freeze({
    ENABLED: 'enabled',
    DISABLED: 'disabled',
    TOGGLE: 'toggle',
});

const states = {
  setup: state(
    transition(TRANSITION_KEYS.ENABLED, 'normal'),
    transition(TRANSITION_KEYS.DISABLED, 'disabled')
  ),
  normal: state(transition(TRANSITION_KEYS.TOGGLE, 'fullscreen')),
  fullscreen: state(transition(TRANSITION_KEYS.TOGGLE, 'normal')),
  disabled: final(),
}

const screen = createMachine('setup', states);

const service = interpret(screen, service => {
  console.log(`SCREEN State: ${service.machine.current}`);
});

export {screen, TRANSITION_KEYS, service}
export default screen;