import {
    createMachine,
    state,
    transition,
    interpret
} from 'robot3';

if (typeof localStorage === "undefined" || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  // eslint-disable-next-line no-global-assign
  localStorage = new LocalStorage('./ui_machine');
}

const TRANSITION_KEYS = Object.freeze({
    SWITCH: 'switch'
});

const states = {
    markdown: state(transition(TRANSITION_KEYS.SWITCH, 'preview')),
    preview: state(transition(TRANSITION_KEYS.SWITCH, 'both')),
    both: state(transition(TRANSITION_KEYS.SWITCH, 'markdown'))
}

const ui = createMachine(localStorage.getItem("ui-state") || 'markdown', states);

const service = interpret(ui, service => {
  const {current} = service.machine
  localStorage.setItem("ui-state", current);
});

export {ui, TRANSITION_KEYS, service}
export default ui;