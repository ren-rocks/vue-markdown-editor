# vue-markdown-editor

![vue-markdown-editor](https://gitlab.com/ren-rocks/vue-markdown-editor/-/raw/develop/public/screenshot.png)

lightweight vue markdown editor component that uses finite state machine model to toggle views and fullscreen windows.

## Usage
Installation:
`npm i @ren-rocks/vue-markdown-editor` or `yarn add @ren-rocks/vue-markdown-editor`

```
...
    <VueMarkdownEditor />
  </div>
</template>

<script>
import VueMarkdownEditor from '@ren-rocks/vue-markdown-editor'

export default {
  name: 'App',
  components: {
    VueMarkdownEditor
  }
...
```

## Contributing
### Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```
